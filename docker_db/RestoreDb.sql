-- RESTORE DATABASE arb_customers FROM DISK = '/var/opt/mssql/backup/arb_customers-2022111-12-30-38.bak'


USE master
GO

PRINT 'Restoring Adventure works'
 -------------------------------------------------
--> Restoring Adventure works
-------------------------------------------------

RESTORE DATABASE arb_customers
FROM DISK =  N'/work/arb_customers.bak'
WITH FILE = 1,
     MOVE N'arb_customers'
     TO  N'/var/opt/mssql/data/arb_customers.mdf',
     MOVE N'arb_customers_log'
     TO  N'/var/opt/mssql/data/arb_customers.ldf',
     NOUNLOAD,
     STATS = 5;

GO

RESTORE DATABASE openbank_apimgtdb
FROM DISK =  N'/work/openbank_apimgtdb.bak'
WITH FILE = 1,
     MOVE N'openbank_apimgtdb'
     TO  N'/var/opt/mssql/data/openbank_apimgtdb.mdf',
     MOVE N'openbank_apimgtdb_log'
     TO  N'/var/opt/mssql/data/openbank_apimgtdb.ldf',
     NOUNLOAD,
     STATS = 5;

GO

RESTORE DATABASE openbank_apimgt_statsdb
FROM DISK =  N'/work/openbank_apimgt_statsdb.bak'
WITH FILE = 1,
     MOVE N'openbank_apimgt_statsdb'
     TO  N'/var/opt/mssql/data/openbank_apimgt_statsdb.mdf',
     MOVE N'openbank_apimgt_statsdb_log'
     TO  N'/var/opt/mssql/data/openbank_apimgt_statsdb.ldf',
     NOUNLOAD,
     STATS = 5;

GO

RESTORE DATABASE openbank_mbstoredb
FROM DISK =  N'/work/openbank_mbstoredb.bak'
WITH FILE = 1,
     MOVE N'openbank_mbstoredb'
     TO  N'/var/opt/mssql/data/openbank_mbstoredb.mdf',
     MOVE N'openbank_mbstoredb_log'
     TO  N'/var/opt/mssql/data/openbank_mbstoredb.ldf',
     NOUNLOAD,
     STATS = 5;

GO

RESTORE DATABASE openbank_userdb
FROM DISK =  N'/work/openbank_userdb.bak'
WITH FILE = 1,
     MOVE N'openbank_userdb'
     TO  N'/var/opt/mssql/data/openbank_userdb.mdf',
     MOVE N'openbank_userdb_log'
     TO  N'/var/opt/mssql/data/openbank_userdb.ldf',
     NOUNLOAD,
     STATS = 5;

GO

RESTORE DATABASE openbank_am_configdb
FROM DISK =  N'/work/openbank_am_configdb.bak'
WITH FILE = 1,
     MOVE N'openbank_am_configdb'
     TO  N'/var/opt/mssql/data/openbank_am_configdb.mdf',
     MOVE N'openbank_am_configdb_log'
     TO  N'/var/opt/mssql/data/openbank_am_configdb.ldf',
     NOUNLOAD,
     STATS = 5;

GO


RESTORE DATABASE openbank_govdb
FROM DISK =  N'/work/openbank_govdb.bak'
WITH FILE = 1,
     MOVE N'openbank_govdb'
     TO  N'/var/opt/mssql/data/openbank_govdb.mdf',
     MOVE N'openbank_govdb_log'
     TO  N'/var/opt/mssql/data/openbank_govdb.ldf',
     NOUNLOAD,
     STATS = 5;

GO

RESTORE DATABASE openbank_tra_db
FROM DISK =  N'/work/openbank_tra_db.bak'
WITH FILE = 1,
     MOVE N'openbank_tra_db'
     TO  N'/var/opt/mssql/data/openbank_tra_db.mdf',
     MOVE N'openbank_tra_db_log'
     TO  N'/var/opt/mssql/data/openbank_tra_db.ldf',
     NOUNLOAD,
     STATS = 5;

GO

RESTORE DATABASE openbank_geolocationdb
FROM DISK =  N'/work/openbank_geolocationdb.bak'
WITH FILE = 1,
     MOVE N'openbank_geolocationdb'
     TO  N'/var/opt/mssql/data/openbank_geolocationdb.mdf',
     MOVE N'openbank_geolocationdb_log'
     TO  N'/var/opt/mssql/data/openbank_geolocationdb.ldf',
     NOUNLOAD,
     STATS = 5;

GO

RESTORE DATABASE openbank_bps_configdb
FROM DISK =  N'/work/openbank_bps_configdb.bak'
WITH FILE = 1,
     MOVE N'openbank_bps_configdb'
     TO  N'/var/opt/mssql/data/openbank_bps_configdb.mdf',
     MOVE N'openbank_bps_configdb_log'
     TO  N'/var/opt/mssql/data/openbank_bps_configdb.ldf',
     NOUNLOAD,
     STATS = 5;

GO

RESTORE DATABASE openbank_bpsdb
FROM DISK =  N'/work/openbank_bpsdb.bak'
WITH FILE = 1,
     MOVE N'openbank_bpsdb'
     TO  N'/var/opt/mssql/data/openbank_bpsdb.mdf',
     MOVE N'openbank_bpsdb_log'
     TO  N'/var/opt/mssql/data/openbank_bpsdb.ldf',
     NOUNLOAD,
     STATS = 5;

GO

RESTORE DATABASE openbank_iskm_configdb
FROM DISK =  N'/work/openbank_iskm_configdb.bak'
WITH FILE = 1,
     MOVE N'openbank_iskm_configdb'
     TO  N'/var/opt/mssql/data/openbank_iskm_configdb.mdf',
     MOVE N'openbank_iskm_configdb_log'
     TO  N'/var/opt/mssql/data/openbank_iskm_configdb.ldf',
     NOUNLOAD,
     STATS = 5;

GO

RESTORE DATABASE openbank_consentdb
FROM DISK =  N'/work/openbank_consentdb.bak'
WITH FILE = 1,
     MOVE N'openbank_consentdb'
     TO  N'/var/opt/mssql/data/openbank_consentdb.mdf',
     MOVE N'openbank_consentdb_log'
     TO  N'/var/opt/mssql/data/openbank_consentdb.ldf',
     NOUNLOAD,
     STATS = 5;

GO

