#!/bin/sh

ARG="$1"

trap_ctrlc ()
{
    echo "Ctrl-C caught...performing shutting down tail logs"
    echo "Stoping tailing"
    kill -9 `ps -ef | grep tail | awk '{print $2}'`
    exit 2
}
trap "trap_ctrlc" 2

echo "tailing all server logs"
if [ -z "$ARG" ]
    then
        echo "No argument supplied"
        exit 2
    else
        case "$ARG" in
            "AM") echo "AM logs opening"
            tail -f ./wso2-obam-1.3.0/repository/logs/wso2carbon.log
            ;;
            "KM") echo "KM logs opening"
            tail -f ./wso2-obkm-1.3.0/repository/logs/wso2carbon.log
            ;;
            "EI") echo "EI logs opening"
            tail -f ./wso2ei-6.4.0/repository/logs/wso2carbon.log
            ;;
            "BPS") echo "BPS logs opening"
            tail -f ./wso2ei-6.4.0/wso2/business-process/repository/logs/wso2carbon.log
            ;;
            "ALL") echo "ALL logs opening"
            tail -f ./wso2-obkm-1.3.0/repository/logs/wso2carbon.log | sed "s/^/$(tput setaf 1)$(tput setab 6)KM Server - $(tput sgr0)/" &
            tail -f ./wso2-obam-1.3.0/repository/logs/wso2carbon.log | sed "s/^/$(tput setaf 1)$(tput setab 3)AM Server - $(tput sgr0)/" &
            tail -f ./wso2ei-6.4.0/repository/logs/wso2carbon.log | sed "s/^/$(tput setaf 7)$(tput setab 1)EI Server - $(tput sgr0)/" &
            tail -f ./wso2ei-6.4.0/wso2/business-process/repository/logs/wso2carbon.log | sed "s/^/$(tput setaf 7)$(tput setab 4)BPS Server - $(tput sgr0)/"
            ;;
        esac
fi
