#!/bin/sh
if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit 2
fi

sh ./wso2-obkm-1.3.0/bin/wso2server.sh $1 &
sh ./wso2-obam-1.3.0/bin/wso2server.sh $1 &
sh ./wso2ei-6.4.0/bin/integrator.sh $1 &
sh ./wso2ei-6.4.0/wso2/business-process/bin/wso2server.sh $1 &

exit 2